<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Name Holder</name>
   <tag></tag>
   <elementGuidId>9749a3c6-692e-419e-b72c-206f7c81f0c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div/form/p[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>48147c22-5f1d-46af-b9a9-5853e9020ec2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>clearfix</value>
      <webElementGuid>56979876-c763-419d-8853-cc3a1d9e4ab1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            Name Holder
                                        
                                        
                                            

                                        


                                    </value>
      <webElementGuid>ff5b1cf5-858f-439e-a518-8a8fdd369da6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;py-4&quot;]/form[1]/p[@class=&quot;clearfix&quot;]</value>
      <webElementGuid>82a6b21b-cfb3-4419-b689-a6f1b11a0eff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div/form/p[2]</value>
      <webElementGuid>ea9d07bd-0a9f-48cb-a3f9-c8669c55bef6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ovo / Gopay'])[1]/following::p[1]</value>
      <webElementGuid>ef51004f-044b-4f18-b026-b6b334fb12c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::p[2]</value>
      <webElementGuid>59e4f909-8bcc-48d8-8f74-12752903433b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Number Holder'])[1]/preceding::p[1]</value>
      <webElementGuid>1e24f051-8023-458a-9995-3a1dc0605d7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[2]</value>
      <webElementGuid>c85024bf-ae45-434f-a071-fcf0dbdb1276</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                                        
                                            Name Holder
                                        
                                        
                                            

                                        


                                    ' or . = '
                                        
                                            Name Holder
                                        
                                        
                                            

                                        


                                    ')]</value>
      <webElementGuid>64f9c27e-1f24-44ba-a36e-9d67f9995eb9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
