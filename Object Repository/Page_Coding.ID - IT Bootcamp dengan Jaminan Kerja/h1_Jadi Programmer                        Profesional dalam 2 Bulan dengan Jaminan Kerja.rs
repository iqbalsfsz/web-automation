<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Jadi Programmer                        Profesional dalam 2 Bulan dengan Jaminan Kerja</name>
   <tag></tag>
   <elementGuidId>4bded0c6-5f16-401e-a3ec-aec7ef71b205</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.new_headline_h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>e097aec0-db4e-4817-8ce7-a789f3794fc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_h3</value>
      <webElementGuid>831d48e8-53ff-4e18-b802-91215993a9b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Jadi Programmer
            
            Profesional dalam 2 Bulan dengan Jaminan Kerja
        </value>
      <webElementGuid>f9aaaa88-0ec9-44e1-8d75-031063b582c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h1[@class=&quot;new_headline_h3&quot;]</value>
      <webElementGuid>1fb4fce0-84d7-4341-b9e6-8e0444a71677</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h1</value>
      <webElementGuid>8ea287f7-9491-48dd-ac2b-b613214bd76c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Engineer Bootcamp'])[2]/preceding::h1[1]</value>
      <webElementGuid>f13565cd-7798-4c44-9040-7c01676f6543</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Jadi Programmer']/parent::*</value>
      <webElementGuid>48c7c21c-6cf8-4f47-866a-0d8a1453f54f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>ebbae8b8-faf8-4896-ad25-d12f24cb7d03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
            Jadi Programmer
            
            Profesional dalam 2 Bulan dengan Jaminan Kerja
        ' or . = '
            Jadi Programmer
            
            Profesional dalam 2 Bulan dengan Jaminan Kerja
        ')]</value>
      <webElementGuid>1ca13036-6d58-4708-8f6e-5d7a6d15e824</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
