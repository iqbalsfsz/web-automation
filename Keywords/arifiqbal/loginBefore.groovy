package arifiqbal

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class loginBefore {
	
	@Keyword
	def loginBeforeEach() {
		WebUI.openBrowser('')
	
		WebUI.navigateToUrl('https://demo-app.online/login')
		
		WebUI.setText(findTestObject('Object Repository/Dashboard/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
			'wondimapiko@gmail.com')
		
		WebUI.setEncryptedText(findTestObject('Object Repository/Dashboard/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
			'OP94eXA/iruRpj9PxJ8uIg==')
		
		WebUI.click(findTestObject('Object Repository/Dashboard/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))
		}
}
