<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Dashboard</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f88e7d35-1dc3-422a-88aa-a6915aa3e951</testSuiteGuid>
   <testCaseLink>
      <guid>0d5d3060-b313-4a5b-93ed-afbaf8a36698</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-01 - Verify Add Billing Info Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0bbb4b66-f523-4782-8933-70c9468235a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-03 - Verify Add Billing Info Failed - Invalid Name Holder (symbol and number)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7f60d315-5ab0-484a-8086-05360482364e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-04 - Verify Add Billing Info Failed - Blank Name Holder Field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0c34f1ff-86f4-41d6-a469-0c6165945c33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-01 - Verify Change Profile Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>15f14fbf-e481-4dd9-bdc1-0eaf46eb60e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-02 - Verify Change Profile Failed - Invalid FullName (symbol and number)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8bd85eff-5d3f-4679-ae97-ad256ca9b41d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-03 - Verify Change Profile Failed - Blank Full Name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>93b24e45-4ff2-45e2-bc23-b9bb247c6fbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-04 - Verify Change Profile Failed - Invalid Phone Number (symbol and letter)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f82a54ca-10f7-430c-83c4-d32bb1f45a0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-05 - Verify Change Profile Failed - Invalid Phone Number (outside of range)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6d6b474d-7303-4439-adc7-aec31f6ce4cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-07 - Verify Change Profile Failed - Blank Phone Number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b8e81807-62ea-4e26-9b22-e5aa66d1e0df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Dashboard/TC-DashB-01 - Verify Go To Dashboard Page Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
